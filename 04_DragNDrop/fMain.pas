unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls;

type
  TForm4 = class(TForm)
    TreeView1: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TreeView1DragOver(Sender, Source: TObject; X, Y: Integer; State:
        TDragState; var Accept: Boolean);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.FormCreate(Sender: TObject);
var
  n1, n2: TTreeNode;
begin
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'A');
  TreeView1.Items.AddChild(n1, 'A1');
  TreeView1.Items.AddChild(n1, 'A2');
  TreeView1.Items.AddChild(n1, 'A3');
  TreeView1.Items.AddChild(n1, 'A4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'B');
  TreeView1.Items.AddChild(n1, 'B1');
  n2 := TreeView1.Items.AddChild(n1, 'B2');
  TreeView1.Items.AddChild(n2, 'B21');
  TreeView1.Items.AddChild(n2, 'B22');
  TreeView1.Items.AddChild(n2, 'B23');
  TreeView1.Items.AddChild(n2, 'B24');
  TreeView1.Items.AddChild(n2, 'B25');
  TreeView1.Items.AddChild(n1, 'B3');
  TreeView1.Items.AddChild(n1, 'B4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'C');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'D');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'E');
  n2 := TreeView1.Items.AddChild(n1, 'E1');
  TreeView1.Items.AddChild(n2, 'E11');
  TreeView1.Items.AddChild(n2, 'E12');
  TreeView1.Items.AddChild(n2, 'E13');
  n2 := TreeView1.Items.AddChild(n1, 'E2');
  TreeView1.Items.AddChild(n2, 'E21');
  TreeView1.Items.AddChild(n2, 'E22');
  TreeView1.Items.AddChild(n2, 'E23');
  TreeView1.Items.AddChild(n2, 'E24');
  TreeView1.Items.AddChild(n1, 'E3');
  n2 := TreeView1.Items.AddChild(n1, 'E4');
  TreeView1.Items.AddChild(n2, 'E41');
  TreeView1.Items.AddChild(n2, 'E42');
  n2 := TreeView1.Items.AddChild(n2, 'E43');
  TreeView1.Items.AddChild(n2, 'E431');
  TreeView1.Items.AddChild(n2, 'E432');
  TreeView1.Items.AddChild(n2, 'E433');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'F');
  TreeView1.Items.AddChild(n1, 'F1');
  TreeView1.Items.AddChild(n1, 'F2');
  TreeView1.Items.AddChild(n1, 'F3');
  TreeView1.Items.AddChild(n1, 'F4');
  TreeView1.FullExpand;

  TreeView1.DragMode := TDragMode.dmAutomatic;
end;

procedure TForm4.TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
var
  node : TTreeNode;
  AttachMode: TNodeAttachMode;
  ht: THitTests;
begin
  if (TreeView1.Selected = nil) then
    Exit;

  ht := TreeView1.GetHitTestInfoAt(X, Y);
  node := TreeView1.GetNodeAt(X, Y);

  if (ht - [htOnRight, htOnItem, htOnIcon, htNowhere, htOnIndent] <> ht) then
  begin
    if (htOnItem in ht) or (htOnRight in ht) or (htOnIcon in ht) then
      AttachMode := naAddChild
    else if (htNowhere in ht) then
      AttachMode := naAdd
    else if (htOnIndent in ht) then
      AttachMode := naInsert
    else
      AttachMode := naAdd; {d�faut Ajout}

    TreeView1.Selected.MoveTo(node, AttachMode);
  end;
end;

procedure TForm4.TreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
    State: TDragState; var Accept: Boolean);
var
  Src, Dst: TTreeNode;
begin
  Accept := (TreeView1.Selected <> TreeView1.GetNodeAt(X,Y));
end;

end.
