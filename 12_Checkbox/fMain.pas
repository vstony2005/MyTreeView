unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, System.ImageList,
  Vcl.ImgList;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    ImageList1: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure Tree1Checked(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1Collapsed(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1Expanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
        Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var
        ImageIndex: TImageIndex);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
    procedure CheckNodeData(ANode: PVirtualNode; AIsExpanded: Boolean);
    procedure BuildTree;
    procedure ProcessSubCheck(Sender: TBaseVirtualTree; Node: PVirtualNode;
        Data: Pointer; var Abort: Boolean);
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

type
  PMyData = ^TMyData;
  TMyData = class
  public
    Text: string[80];
    IsCheck: Boolean;
    IsExpanded: Boolean;
  end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  BuildTree;
  Tree1.FullExpand;
end;

{$REGION 'TreeEvents'}
procedure TForm1.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  ldata: TMyData;
begin
  ldata := TMyData.Create;
  Node.SetData(ldata);
end;

procedure TForm1.Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  ldata: PMyData;
begin
  ldata := Node.GetData;
  if Assigned(ldata) then
    ldata.Free;
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  ldata: PMyData;
begin
  if not Assigned(Node) then
    Exit;

  ldata := Node.GetData;
  if Assigned(ldata) then
    CellText := ldata.Text;
end;

procedure TForm1.Tree1Collapsed(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  CheckNodeData(Node, False);
end;

procedure TForm1.Tree1Expanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  CheckNodeData(Node, True);
end;

procedure TForm1.Tree1GetImageIndex(Sender: TBaseVirtualTree; Node:
    PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted:
    Boolean; var ImageIndex: TImageIndex);
var
  ldata: PMyData;
begin
if (Kind = ikNormal) or (Kind = ikSelected) then

  if (Tree1.GetNodeLevel(Node) = 0) then
  begin
    ldata := Node.GetData;
    if Assigned(ldata) and (ldata.IsExpanded) then
      ImageIndex := 1
    else
      ImageIndex := 0;
  end
  else
    ImageIndex := -1;
end;

procedure TForm1.Tree1Checked(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  Tree1.IterateSubtree(Node, ProcessSubCheck, Pointer(Node.CheckState), []);
end;
{$ENDREGION}

procedure TForm1.BuildTree;
var
  i, j: Integer;
  n1, n2: PVirtualNode;
  ldata: PMyData;
begin
  Tree1.NodeDataSize := SizeOf(TMyData);
  Tree1.TreeOptions.MiscOptions := Tree1.TreeOptions.MiscOptions
                                   + [TVTMiscOption.toCheckSupport];
  for i := 1 to 10 do
  begin
    n1 := Tree1.AddChild(nil);
    n1.CheckType := ctCheckBox;
    ldata := n1.GetData;
    ldata.Text := Format('Table %d', [i]);
    for j := 1 to 10 do
    begin
      n2 := Tree1.AddChild(n1);
      n2.CheckType := ctCheckBox;
      ldata := n2.GetData;
      ldata.Text := Format('%d x %d = %d', [i, j, i*j]);
    end;
  end;
end;

procedure TForm1.ProcessSubCheck(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Data: Pointer; var Abort: Boolean);
begin
  Node.CheckState := TCheckState(Data);
end;

procedure TForm1.CheckNodeData(ANode: PVirtualNode; AIsExpanded: Boolean);
var
  ldata: PMyData;
begin
  if not Assigned(ANode) then
    Exit;
  ldata := ANode.GetData;
  if Assigned(ldata) then
  begin
    ldata.IsExpanded := AIsExpanded;
  end;
end;

end.
