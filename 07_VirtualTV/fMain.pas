unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    btnAddRoot: TButton;
    btnClear: TButton;
    Label1: TLabel;
    btnAddChild: TButton;
    edtNbNode: TEdit;
    procedure btnAddClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

type
  PMyRec = ^TMyRec;
  TMyRec = record
    Caption: WideString;
  end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Tree1.NodeDataSize := SizeOf(TMyRec);
  Tree1.RootNodeCount := 20;

  btnAddRoot.Tag := 0;
  btnAddChild.Tag := 1;

  edtNbNode.Text := '3';
end;

procedure TForm1.btnAddClick(Sender: TObject);
var
  count, start: Cardinal;
  curs: TCursor;
begin
  curs := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    start := GetTickCount;

    count := StrToIntDef(edtNbNode.Text, 0);

    if (count > 0) then
    case (Sender as TButton).Tag of
      0:  // add root
        Tree1.RootNodeCount := Tree1.RootNodeCount + count;
      1:  // add child
        if Assigned(Tree1.FocusedNode) then
        begin
          Tree1.ChildCount[Tree1.FocusedNode] := Tree1.ChildCount[Tree1.FocusedNode] + count;
          Tree1.Expanded[Tree1.FocusedNode] := True;
          Tree1.InvalidateToBottom(Tree1.FocusedNode);
        end;
    end;

    Label1.Caption := Format('Last operation duration: %d ms', [GetTickCount - Start]);
  finally
    Screen.Cursor := curs;
  end;
end;

procedure TForm1.btnClearClick(Sender: TObject);
var
  start: Cardinal;
  curs: TCursor;
begin
  curs := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    start := GetTickCount;
    Tree1.Clear;
    Label1.Caption := Format('Last operation duration: %d ms', [GetTickCount - Start]);
  finally
    Screen.Cursor := curs;
  end;
end;

procedure TForm1.Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  data: PMyRec;
begin
  data := Sender.GetNodeData(Node);
  Finalize(data^);
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  data: PMyRec;
begin
  data := Sender.GetNodeData(Node);
  if Assigned(data) then
    CellText := data.Caption;
end;

procedure TForm1.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  data: PMyRec;
begin
  data := Sender.GetNodeData(Node);
  data.Caption := Format('Level %d, Index %d', [Tree1.GetNodeLevel(Node), Node.Index]);
end;

end.
