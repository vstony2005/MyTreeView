object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 440
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    624
    440)
  TextHeight = 15
  object Label1: TLabel
    Left = 280
    Top = 411
    Width = 34
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Label1'
  end
  object Tree1: TVirtualStringTree
    Left = 16
    Top = 8
    Width = 585
    Height = 385
    Anchors = [akLeft, akTop, akRight, akBottom]
    Header.AutoSizeIndex = 0
    Header.MainColumn = -1
    TabOrder = 0
    OnFreeNode = Tree1FreeNode
    OnGetText = Tree1GetText
    OnInitNode = Tree1InitNode
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    Columns = <>
  end
  object btnAddRoot: TButton
    Left = 16
    Top = 407
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Add root'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object btnClear: TButton
    Left = 178
    Top = 407
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Clear'
    TabOrder = 2
    OnClick = btnClearClick
  end
  object btnAddChild: TButton
    Tag = 1
    Left = 97
    Top = 407
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Add Child'
    TabOrder = 3
    OnClick = btnAddClick
  end
  object edtNbNode: TEdit
    Left = 536
    Top = 409
    Width = 65
    Height = 23
    Anchors = [akRight, akBottom]
    TabOrder = 4
  end
end
