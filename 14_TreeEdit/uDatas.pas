unit uDatas;

interface

type
  TValueType = (
    vtNone,
    vtString,
    vtPickString,
    vtNumber,
    vtPickNumber,
    vtMemo,
    vtDate,
    vtButton
  );

  PMyDatas = ^TMyDatas;
  TMyDatas = class
  public
    ValueType: TValueType;
    Value: string;
    Changed: Boolean;
    Locked: Boolean;
  end;

implementation

end.
