unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees;

type
  TFrmMain = class(TForm)
    Tree1: TVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure Tree1Change(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1CreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
        Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure Tree1Editing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; var Allowed: Boolean);
    procedure Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
  public
    procedure BuildTree;
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  uTreeEditor, uDatas;

const
  // Helper message to decouple node change handling from edit handling.
  WM_STARTEDITING = WM_USER + 778;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  BuildTree;
end;

{$REGION 'TreeView'}
procedure TFrmMain.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  ldata: TMyDatas;
begin
  ldata := TMyDatas.Create;
  Node.SetData(ldata);
end;

procedure TFrmMain.Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  ldata: PMyDatas;
begin
  ldata := Node.GetData;
  ldata.Free;
end;

procedure TFrmMain.Tree1CreateEditor(Sender: TBaseVirtualTree; Node:
    PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  EditLink := TTreeEditor.Create;
end;

procedure TFrmMain.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  ldata: PMyDatas;
begin
  ldata := Node.GetData;
  CellText := ldata.Value;
end;

procedure TFrmMain.Tree1Editing(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; var Allowed: Boolean);
var
  ldata: PMyDatas;
begin
  ldata := Node.GetData;
  Allowed := ldata.ValueType <> vtNone;
end;
{$ENDREGION}

procedure TFrmMain.BuildTree;
var
  n1, n2: PVirtualNode;
  i, j: Integer;
  ldata: PMyDatas;
begin
  Tree1.NodeDataSize := SizeOf(TMyDatas);
  for i := 0 to 2 do
  begin
    n1 := Tree1.AddChild(nil);
    ldata := n1.GetData;
    ldata.Value := Format('Level %d', [i+1]);

    for j  := 0 to 5 do
    begin
      n2 := Tree1.AddChild(n1);
      ldata := n2.GetData;

      case j of
        0:
          begin
            ldata.ValueType := vtPickString;
            ldata.Value := 'Zero';
          end;
        1:
          begin
            ldata.ValueType := vtDate;
            ldata.Value := FormatDateTime('dd/mm/yyyy', Now);
          end;
        2:
          begin
            ldata.ValueType := vtNumber;
            ldata.Value := IntToStr(i * j);
          end
      else
        ldata.ValueType := vtString;
        ldata.Value := Format('Sub %d', [j]);
      end;
    end;
  end;

  Tree1.FullExpand;
end;

procedure TFrmMain.Tree1Change(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  if Assigned(Node) and Assigned(Node.Parent) then
    PostMessage(Self.Handle, WM_STARTEDITING, WPARAM(Node), 0);
end;

end.
