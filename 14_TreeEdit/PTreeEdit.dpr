program PTreeEdit;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uMain in 'uMain.pas' {FrmMain},
  uTreeEditor in 'uTreeEditor.pas',
  uDatas in 'uDatas.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
