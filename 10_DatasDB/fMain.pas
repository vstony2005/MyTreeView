unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, VirtualTrees, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    con1: TFDConnection;
    qry1: TFDQuery;
    driver1: TFDPhysMSSQLDriverLink;
    btnExpanded: TButton;
    btnCollapse: TButton;
    edtPrice: TEdit;
    edtStock: TEdit;
    btnUpdate: TButton;
    ImageList1: TImageList;
    procedure btnCollapseClick(Sender: TObject);
    procedure btnExpandedClick(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Tree1BeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
        Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode;
        CellRect: TRect; var ContentRect: TRect);
    procedure Tree1Change(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1DrawText(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node:
        PVirtualNode; Column: TColumnIndex; const Text: string; const CellRect:
        TRect; var DefaultDraw: Boolean);
    procedure Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
        Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var
        ImageIndex: TImageIndex);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
    procedure LoadDatas;
    procedure InitTree;
    procedure BuildTree;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  System.AnsiStrings;

type
  PMyData = ^TMyData;
  TMyData = class
    Level: Integer;
    Text: string[40];
    Category: string[15];
    Product: string[40];
    Price: Extended;
    Stock: Integer;
    Valid: Boolean;
  end;

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  BuildTree;
end;

{$REGION 'Buttons'}
procedure TForm1.btnCollapseClick(Sender: TObject);
begin
  Tree1.FullCollapse;
end;

procedure TForm1.btnExpandedClick(Sender: TObject);
begin
  Tree1.FullExpand;
end;

procedure TForm1.btnUpdateClick(Sender: TObject);
var
  n: PVirtualNode;
  mdata: PMyData;
begin
  n := Tree1.FocusedNode;
  if not Assigned(n) then
    Exit;

  mdata := n.GetData;
  if not Assigned(mdata) then
    Exit;

  if (mdata.Level > 2) then
  begin
    mdata.Price := StrToFloatDef(edtPrice.Text, mdata.Price);
    mdata.Stock := StrToIntDef(edtStock.Text, mdata.Stock);
    mdata.Valid := (mdata.Stock > 0);
    Tree1.RepaintNode(n);
  end;
end;
{$ENDREGION}

{$REGION 'Treeview'}
procedure TForm1.Tree1Change(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  mdata: PMyData;
begin
  if not Assigned(Node) then
    Exit;

  mdata := Node.GetData;
  if Assigned(mdata) and (mdata.Level > 2) then
  begin
    edtPrice.Text := CurrToStrF(mdata.Price, ffcurrency, 2, FormatSettings);
    edtStock.Text := IntToStr(mdata.Stock);
  end
  else
  begin
    edtPrice.Text := string.Empty;
    edtStock.Text := string.Empty;
  end;
end;

procedure TForm1.Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  mdata: PMyData;
begin
  mdata := Node.GetData;
  if Assigned(mdata) then
    mdata.Free;
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  mdata: PMyData;
begin
  mdata := Node.GetData;
  if Assigned(mdata) then
  begin
    if (mdata.Level = 1) or (mdata.Level = 2) then
    begin
      if (Column = 0) then
        CellText := mdata.Text
      else
        CellText := '';
    end
    else
      case Column of
        1: CellText := mdata.Product;
        2: CellText := CurrToStrF(mdata.Price, ffcurrency, 2, FormatSettings);
        3: CellText := IntToStr(mdata.Stock);
      else
        CellText := '';
      end;
  end;
end;

procedure TForm1.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  mdata: TMyData;
begin
  mdata := TMyData.Create;
  Node.SetData(mdata);
end;

procedure TForm1.Tree1DrawText(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
    Node: PVirtualNode; Column: TColumnIndex; const Text: string; const
    CellRect: TRect; var DefaultDraw: Boolean);
var
  mdata: PMyData;
begin
  mdata := Node.GetData;
  if Assigned(mdata) then
  begin
    if (mdata.Level = 2) then
      TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold]
    else if (mdata.Level > 2) and (mdata.Stock = 0) then
    begin
      TargetCanvas.Brush.Color := clYellow;
      TargetCanvas.Font.Color := clRed;
    end;
  end;
end;

procedure TForm1.Tree1BeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas:
    TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellPaintMode:
    TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
var
  mdata: PMyData;
begin
  mdata := Node.GetData;
  if Assigned(mdata) then
  begin
    if (mdata.Level > 2) and (mdata.Stock = 0) then
    begin
      TargetCanvas.Brush.Color := clYellow;
      TargetCanvas.FillRect(CellRect);
    end;
  end;
end;

procedure TForm1.Tree1GetImageIndex(Sender: TBaseVirtualTree; Node:
    PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted:
    Boolean; var ImageIndex: TImageIndex);
var
  mdata: PMyData;
begin
  if (Kind in [ikNormal, ikSelected])
    and (Column = 4) then
  begin
    mdata := Node.GetData;
    if Assigned(mdata) and (mdata.Level = 3) then
    begin
      if (mdata.Valid) then
        ImageIndex := 1
      else
        ImageIndex := 0;
    end;
  end;
end;
{$ENDREGION}

procedure TForm1.LoadDatas;
begin
  try
    con1.DriverName := 'MSSQL';
    con1.LoginPrompt := False;

    con1.Params.Values['Server'] := '.';
    con1.Params.Database := 'Northwind';
    con1.Params.UserName := 'sa';
    con1.Params.Password := 'passwd';

    con1.Connected := True;
    qry1.SQL.Text := 'SELECT '
                      + '  c.CategoryName,'
                      + '  p.ProductName,'
                      + '  p.UnitPrice,'
                      + '  p.UnitsInStock '
                      + 'FROM dbo.Products p '
                      + 'LEFT JOIN dbo.Categories c ON p.CategoryID = c.CategoryID '
                      + 'ORDER BY '
                      + '  c.CategoryName,'
                      + '  p.ProductName;';
    qry1.Open;
  except
    on e: Exception do
      MessageDlg(Format('Error: %s',[e.Message]),
                 TMsgDlgType.mtError,
                 [TMsgDlgBtn.mbOK],0);
  end;
end;

procedure TForm1.InitTree;
var
  col: TVirtualTreeColumn;
begin
  Tree1.NodeDataSize := SizeOf(TMyData);

  col := Tree1.Header.Columns.Add;
  col.Text := 'Category';
  col.Width := 120;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Product';
  col.Width := 200;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Price';
  col.Width := 80;
  col.Alignment := TAlignment.taRightJustify;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Stock';
  col.Hint := 'Units in stock';
  col.Width := 80;
  col.Alignment := TAlignment.taRightJustify;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Valid';
  col.Width := 50;
  col.Alignment := TAlignment.taCenter;

  Tree1.Header.Options := Tree1.Header.Options
                          + [TVTHeaderOption.hoVisible];   // afficher colonnes
  Tree1.TreeOptions.SelectionOptions := Tree1.TreeOptions.SelectionOptions
                                        + [TVTSelectionOption.toFullRowSelect];
end;

procedure TForm1.BuildTree;
var
  currCat, currProd: string;
  nodeCat, nodeProd, n1: PVirtualNode;
  fCat, fProd, fPrice, fStock: TField;
  mdata: PMyData;
begin
  LoadDatas;
  InitTree;

  fCat := qry1.FieldByName('CategoryName');
  fProd := qry1.FieldByName('ProductName');
  fPrice := qry1.FieldByName('UnitPrice');
  fStock := qry1.FieldByName('UnitsInStock');

  currCat := string.Empty;
  currProd := string.Empty;

  while (not qry1.Eof) do
  begin
    if (currCat <> fCat.AsString) then
    begin
      currCat := string.Empty;
      currProd := string.Empty;
    end
    else if (currProd <> (LeftStr(fProd.AsString, 1))) then
    begin
      currProd := string.Empty;
    end;

    if (currCat = string.Empty) then
    begin
      currCat := fCat.AsString;
      nodeCat := Tree1.AddChild(nil);
      mdata := Tree1.GetNodeData(nodeCat);
      mdata.Level := 1;
      mdata.Text := currCat;
    end;

    if (currProd = string.Empty) then
    begin
      currProd := LeftStr(fProd.AsString, 1);
      nodeProd := Tree1.AddChild(nodeCat);
      mdata := Tree1.GetNodeData(nodeProd);
      mdata.Level := 2;
      mdata.Text := Format('[%s]', [currProd]);
    end;


    n1 := Tree1.AddChild(nodeProd);
    mdata := Tree1.GetNodeData(n1);
    mdata.Level := 3;
    mdata.Category := fCat.AsString;
    mdata.Product := fProd.AsString;
    mdata.Price := fPrice.AsFloat;
    mdata.Stock := fStock.AsInteger;
    mdata.Valid := (mdata.Stock > 0);

    qry1.Next;
  end;

  qry1.Close;
end;

end.
