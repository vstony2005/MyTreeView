object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    624
    441)
  TextHeight = 15
  object TreeView1: TTreeView
    Left = 8
    Top = 16
    Width = 601
    Height = 377
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    TabOrder = 0
  end
  object btnOutdent: TButton
    Left = 8
    Top = 408
    Width = 81
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '<< Outdent'
    TabOrder = 1
    OnClick = btnOutdentClick
  end
  object btnIndent: TButton
    Left = 95
    Top = 408
    Width = 82
    Height = 25
    Caption = 'Indent >>'
    TabOrder = 2
    OnClick = btnIndentClick
  end
end
