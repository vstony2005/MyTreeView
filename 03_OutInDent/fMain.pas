unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm3 = class(TForm)
    TreeView1: TTreeView;
    btnOutdent: TButton;
    btnIndent: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnIndentClick(Sender: TObject);
    procedure btnOutdentClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.FormCreate(Sender: TObject);
var
  n1, n2: TTreeNode;
begin
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'A');
  TreeView1.Items.AddChild(n1, 'A1');
  TreeView1.Items.AddChild(n1, 'A2');
  TreeView1.Items.AddChild(n1, 'A3');
  TreeView1.Items.AddChild(n1, 'A4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'B');
  TreeView1.Items.AddChild(n1, 'B1');
  n2 := TreeView1.Items.AddChild(n1, 'B2');
  TreeView1.Items.AddChild(n2, 'B21');
  TreeView1.Items.AddChild(n2, 'B22');
  TreeView1.Items.AddChild(n2, 'B23');
  TreeView1.Items.AddChild(n2, 'B24');
  TreeView1.Items.AddChild(n2, 'B25');
  TreeView1.Items.AddChild(n1, 'B3');
  TreeView1.Items.AddChild(n1, 'B4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'C');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'D');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'E');
  n2 := TreeView1.Items.AddChild(n1, 'E1');
  TreeView1.Items.AddChild(n2, 'E11');
  TreeView1.Items.AddChild(n2, 'E12');
  TreeView1.Items.AddChild(n2, 'E13');
  n2 := TreeView1.Items.AddChild(n1, 'E2');
  TreeView1.Items.AddChild(n2, 'E21');
  TreeView1.Items.AddChild(n2, 'E22');
  TreeView1.Items.AddChild(n2, 'E23');
  TreeView1.Items.AddChild(n2, 'E24');
  TreeView1.Items.AddChild(n1, 'E3');
  n2 := TreeView1.Items.AddChild(n1, 'E4');
  TreeView1.Items.AddChild(n2, 'E41');
  TreeView1.Items.AddChild(n2, 'E42');
  n2 := TreeView1.Items.AddChild(n2, 'E43');
  TreeView1.Items.AddChild(n2, 'E431');
  TreeView1.Items.AddChild(n2, 'E432');
  TreeView1.Items.AddChild(n2, 'E433');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'F');
  TreeView1.Items.AddChild(n1, 'F1');
  TreeView1.Items.AddChild(n1, 'F2');
  TreeView1.Items.AddChild(n1, 'F3');
  TreeView1.Items.AddChild(n1, 'F4');
  TreeView1.FullExpand;
end;

procedure TForm3.btnOutdentClick(Sender: TObject);
var
  node: TTreeNode;
  parentIdx: Integer;
begin
  if (TreeView1.Items.Count = 0) then
    Exit;

  node := TreeView1.Selected;
  node := node.Parent;
  if not Assigned(node) then
  begin
    MessageDlg('This item is already fully outdented!',
               TMsgDlgType.mtWarning,
               [TMsgDlgBtn.mbOK],0);
    Exit;
  end;
  parentIdx := node.AbsoluteIndex;

  if (parentIdx < 0) then
    MessageDlg('This item is already fully outdented!',
               TMsgDlgType.mtWarning,
               [TMsgDlgBtn.mbOK],0)
  else
    TreeView1.Selected.MoveTo(TreeView1.Items[parentIdx], naAdd);

  TreeView1.SetFocus;
end;

procedure TForm3.btnIndentClick(Sender: TObject);
var
  node: TTreeNode;
  sibIdx: Integer;
begin
  if (TreeView1.Items.Count = 0) then
    Exit;

  node := TreeView1.Selected;
  node := node.getPrevSibling;
  if not Assigned(node) then
  begin
    MessageDlg('There is nothing to indent this item beneath!',
               TMsgDlgType.mtWarning,
               [TMsgDlgBtn.mbOK],0);
    Exit;
  end;

  sibIdx := node.AbsoluteIndex;

  if (sibIdx < 0) then
    sibIdx := node.getNextSibling.AbsoluteIndex;
  if (sibIdx < 0) then
    MessageDlg('There is nothing to indent this item beneath!',
               TMsgDlgType.mtWarning,
               [TMsgDlgBtn.mbOK],0)
  else
    TreeView1.Selected.MoveTo(TreeView1.Items.item[sibIdx], naAddChild);

  TreeView1.SetFocus;
end;

end.
