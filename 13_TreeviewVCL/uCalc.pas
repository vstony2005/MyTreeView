unit uCalc;

interface

type
  PCalculElt = ^TCalculElt;
  TCalculElt = class
  private
    FDescription: string;
    FLevel: Integer;
    FN1: Integer;
    FN2: Integer;
    FOperation: Char;
    FResultat: Extended;
    procedure SetN1(const Value: Integer);
    procedure SetN2(const Value: Integer);
    procedure SetOperation(const Value: Char);
    procedure SetResultat(const Value: Extended);
    function GetResultat: Extended;
    procedure SetDescription(const Value: string);
    procedure SetLevel(const Value: Integer);
  public
    property Description: string read FDescription write SetDescription;
    property Level: Integer read FLevel write SetLevel;
    property N1: Integer read FN1 write SetN1;
    property N2: Integer read FN2 write SetN2;
    property Operation: Char read FOperation write SetOperation;
    property Resultat: Extended read GetResultat write SetResultat;
  end;

implementation

uses
  System.Math;

procedure TCalculElt.SetN1(const Value: Integer);
begin
  FN1 := Value;
end;

procedure TCalculElt.SetN2(const Value: Integer);
begin
  FN2 := Value;
end;

procedure TCalculElt.SetOperation(const Value: Char);
begin
  FOperation := Value;
end;

procedure TCalculElt.SetResultat(const Value: Extended);
begin
  FResultat := Value;
end;

function TCalculElt.GetResultat: Extended;
begin
  Result := RoundTo(FResultat, -2);
end;

procedure TCalculElt.SetDescription(const Value: string);
begin
  FDescription := Value;
end;

procedure TCalculElt.SetLevel(const Value: Integer);
begin
  FLevel := Value;
end;

end.
