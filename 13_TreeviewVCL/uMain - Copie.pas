unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, Vcl.StdCtrls;

type
  TFrmMain = class(TForm)
    treev: TVirtualStringTree;
    ListBox1: TListBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure treevGetCellText(Sender: TCustomVirtualStringTree; var E:
        TVSTGetCellTextEventArgs);
  private
    FList: TStringList;
    FLstHead: TStringList;
    procedure LoadDatas;
    procedure LoadTreeView;
    procedure ControlDatas;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  uCalc;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  FList := TStringList.Create;
  FLstHead := TStringList.Create;
  LoadDatas;
  ControlDatas;
  LoadTreeView;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
var
  i: Integer;
  lcalc: TCalculElt;
begin
  for i := FList.Count - 1 downto 0 do
  begin
    lcalc := FList.Objects[i] as TCalculElt;
    lcalc.Free;
  end;
  for i := FLstHead.Count - 1 downto 0 do
  begin
    lcalc := FLstHead.Objects[i] as TCalculElt;
    lcalc.Free;
  end;

  FList.Free;
  FLstHead.Free;
end;

procedure TFrmMain.LoadDatas;
const
  OPS: array[1..4] of Char = ('+', '-', '*', '/');
  OPS_NAMES: array[1..4] of string = ('Addition',
                                      'Substraction',
                                      'Multiplication',
                                      'Division');
var
  o, i, j: Integer;
  calc: TCalculElt;
  c: Char;
begin
  FList.Clear;
  for o := 1 to 4 do
    for i := 1 to 10 do
      for j := 1 to 10 do
      begin
        calc := TCalculElt.Create;
        calc.Description := OPS_NAMES[o];
        calc.N1 := i;
        calc.N2 := j;
        calc.Operation := OPS[o];

        case o of
          1:
            calc.Resultat := i + j;
          2:
            calc.Resultat := i - j;
          3:
            calc.Resultat := i * j;
          4:
            calc.Resultat := i / j;
        end;

        FList.AddObject(calc.Description, calc);
      end;
end;

procedure TFrmMain.ControlDatas;
var
  i: Integer;
  calc: TCalculElt;
begin
  ListBox1.Clear;
  for i := 0 to FList.Count - 1 do
  begin
    calc := FList.Objects[i] as TCalculElt;
    ListBox1.Items.Add(Format(
        '%s: %d %s %d = %s',
        [FList[i],
         calc.N1,
         calc.Operation,
         calc.N2,
         FloatToStr(calc.Resultat)]));
  end;
end;

procedure TFrmMain.LoadTreeView;
var
  curOp: string;
  i: Integer;
  lCalc, lRootCalc: TCalculElt;
  lRootNode, lnode: PVirtualNode;
begin
  curOp := '';
  try
    treev.BeginUpdate;
    treev.Clear;

    for i := 0 to FList.Count - 1 do
    begin
      lCalc := FList.Objects[i] as TCalculElt;
      lCalc.Level := 2;

      if (lCalc.Description <> curOp) then
      begin
        curOp := lCalc.Description;
        lRootCalc := TCalculElt.Create;
        FLstHead.AddObject('', lRootCalc);
        lRootCalc.Level := 1;
        lRootCalc.Description := curOp;

        lRootNode := treev.AddChild(nil);
        lRootNode.SetData(lRootCalc);
      end;

      lnode := treev.AddChild(lRootNode);
      lnode.SetData(lcalc);
    end;
  finally
    treev.EndUpdate;
  end;
end;

procedure TFrmMain.treevGetCellText(Sender: TCustomVirtualStringTree; var E:
    TVSTGetCellTextEventArgs);
var
  calc: PCalculElt;
begin
  calc := Sender.GetNodeData(E.Node);
  if not Assigned(calc) then
    e.CellText := 'Error'
  else if (calc.Level = 0) then
    e.CellText := 'Error level'
  else if (calc.Level = 1) and (E.Column = 0) then
    e.CellText := calc.Description
  else if (calc.Level = 2) then
    case E.Column of
      0:
        E.CellText := calc.N1.ToString;
      1:
        E.CellText := calc.Operation;
      2:
        E.CellText := calc.N2.ToString;
      3:
        E.CellText := '=';
      4:
        E.CellText := FloatToStr(calc.Resultat);
    else
      E.CellText := 'Empty';
    end
  else
    E.CellText := '';
end;

end.
