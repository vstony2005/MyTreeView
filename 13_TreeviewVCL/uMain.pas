unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, Vcl.StdCtrls;

type
  TFrmMain = class(TForm)
    treev: TVirtualStringTree;
    ListBox1: TListBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure treevFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure treevGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
  private
    FList: TStringList;
    procedure LoadDatas;
    procedure LoadTreeView;
    procedure LoadListBox;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  uCalc;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  FList := TStringList.Create;
  LoadDatas;
  LoadListBox;
  LoadTreeView;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
begin
  FList.Free;
end;

procedure TFrmMain.LoadDatas;
const
  OPS: array[1..4] of Char = ('+', '-', '*', '/');
  OPS_NAMES: array[1..4] of string = ('Addition',
                                      'Substraction',
                                      'Multiplication',
                                      'Division');
var
  o, i, j: Integer;
  calc: TCalculElt;
  c: Char;
begin
  FList.Clear;
  for o := 1 to 4 do
    for i := 1 to 10 do
      for j := 1 to 10 do
      begin
        calc := TCalculElt.Create;
        calc.Description := OPS_NAMES[o];
        calc.N1 := i;
        calc.N2 := j;
        calc.Operation := OPS[o];

        case o of
          1:
            calc.Resultat := i + j;
          2:
            calc.Resultat := i - j;
          3:
            calc.Resultat := i * j;
          4:
            calc.Resultat := i / j;
        end;

        FList.AddObject(calc.Description, calc);
      end;
end;

procedure TFrmMain.LoadListBox;
var
  i: Integer;
  calc: TCalculElt;
  currOp: string;
begin
  ListBox1.Clear;
  currOp := string.Empty;
  for i := 0 to FList.Count - 1 do
  begin
    calc := FList.Objects[i] as TCalculElt;

    if (currOp <> calc.Operation) then
    begin
      currOp := calc.Operation;
      if (i > 0) then
        ListBox1.Items.Add('---------');
    end;

    if (calc.N1 > 1) and (calc.N2 = 1) then
      ListBox1.Items.Add('---');

    ListBox1.Items.Add(Format(
        '%s: %d %s %d = %s',
        [FList[i],
         calc.N1,
         calc.Operation,
         calc.N2,
         FloatToStr(calc.Resultat)]));
  end;
end;

procedure TFrmMain.LoadTreeView;
var
  curOp: string;
  i: Integer;
  lCalc, lRootCalc: TCalculElt;
  lRootNode, lnode: PVirtualNode;
begin
  curOp := '';
  try
    treev.BeginUpdate;
    treev.Clear;

    for i := 0 to FList.Count - 1 do
    begin
      lCalc := FList.Objects[i] as TCalculElt;
      lCalc.Level := 2;

      if (lCalc.Description <> curOp) then
      begin
        curOp := lCalc.Description;
        lRootCalc := TCalculElt.Create;
        lRootCalc.Level := 1;
        lRootCalc.Description := curOp;

        lRootNode := treev.AddChild(nil);
        lRootNode.SetData(lRootCalc);
      end;

      lnode := treev.AddChild(lRootNode);
      lnode.SetData(lcalc);
    end;
  finally
    treev.EndUpdate;
  end;
end;

procedure TFrmMain.treevFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  lcalc: PCalculElt;
begin
  lcalc := Node.GetData;
  lcalc.Free;
end;

procedure TFrmMain.treevGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  calc: PCalculElt;
begin
  if Assigned(Node) then
  begin
    calc := Node.GetData;
    if not Assigned(calc) then
      CellText := 'Error'
    else if (calc.Level = 0) then
      CellText := 'Error level'
    else if (calc.Level = 1) and (Column = 0) then
      CellText := calc.Description
    else if (calc.Level = 2) then
      case Column of
        0:
          CellText := calc.N1.ToString;
        1:
          CellText := calc.Operation;
        2:
          CellText := calc.N2.ToString;
        3:
          CellText := '=';
        4:
          CellText := FloatToStr(calc.Resultat);
      else
        CellText := 'Empty';
      end
    else
      CellText := '';
  end;
end;

end.
