object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'FrmMain'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object treev: TVirtualStringTree
    Left = 0
    Top = 0
    Width = 472
    Height = 299
    Align = alClient
    Header.AutoSizeIndex = 0
    TabOrder = 0
    OnFreeNode = treevFreeNode
    OnGetText = treevGetText
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    Columns = <
      item
        Position = 0
        Text = 'Value 1'
        Width = 100
      end
      item
        Position = 1
        Text = 'Operation'
      end
      item
        Position = 2
        Text = 'Value 2'
      end
      item
        Position = 3
        Text = '-'
      end
      item
        Position = 4
        Text = 'Result'
      end>
  end
  object ListBox1: TListBox
    Left = 472
    Top = 0
    Width = 163
    Height = 299
    Align = alRight
    ItemHeight = 13
    TabOrder = 1
  end
end
