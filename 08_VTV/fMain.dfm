object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    624
    441)
  TextHeight = 15
  object Tree1: TVirtualStringTree
    Left = 16
    Top = 8
    Width = 585
    Height = 329
    Anchors = [akLeft, akTop, akRight, akBottom]
    Header.AutoSizeIndex = 0
    Header.MainColumn = -1
    TabOrder = 0
    OnCompareNodes = Tree1CompareNodes
    OnFreeNode = Tree1FreeNode
    OnGetText = Tree1GetText
    OnInitNode = Tree1InitNode
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    Columns = <>
  end
end
