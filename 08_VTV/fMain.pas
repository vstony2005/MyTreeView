unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure Tree1CompareNodes(Sender: TBaseVirtualTree; Node1, Node2:
        PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
    procedure InitTree;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

type
  PMyData = ^TMyData;
  TMyData = class
    id: Integer;
    code: string[40];
    name: string[255];
    valid: Boolean;
  end;

var
  DataId: Integer;

procedure TForm1.FormCreate(Sender: TObject);
begin
  InitTree;
end;

procedure TForm1.InitTree;
var
  col: TVirtualTreeColumn;
begin
  col := Tree1.Header.Columns.Add;
  col.Text := 'Star Event';
  col.Hint := 'Number of star';
  col.Width := 120;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Date';
  col.Hint := 'Date of the Event';
  col.Width := 100;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Charity event name';
  col.Width := 180;

  col := Tree1.Header.Columns.Add;
  col.Text := 'Amount collected';
  col.Width := 180;

  Tree1.Header.Options := Tree1.Header.Options
                          + [TVTHeaderOption.hoVisible,
                             TVTHeaderOption.hoHeaderClickAutoSort];
  Tree1.TreeOptions.PaintOptions := Tree1.TreeOptions.PaintOptions
                                    - [TVTPaintOption.toShowRoot,
                                       TVTPaintOption.toShowTreeLines];
  Tree1.TreeOptions.SelectionOptions := Tree1.TreeOptions.SelectionOptions
                                        + [TVTSelectionOption.toFullRowSelect];

  Tree1.RootNodeCount := 12;

  Tree1.Header.SortDirection := sdDescending;
  Tree1.Header.SortColumn := 1;
end;

procedure TForm1.Tree1CompareNodes(Sender: TBaseVirtualTree; Node1, Node2:
    PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  d1, d2: PMyData;
begin
  d1 := Node1.GetData;
  d2 := Node2.GetData;
  case Column of
    1: Result := CompareText(d1.code, d2.code);
    2: Result := CompareText(d1.name, d2.name);
  else
    Result := 0;
  end;
end;

procedure TForm1.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  data: TMyData;
begin
  data := TMyData.Create;
  Inc(DataId);
  data.id := DataId;
  data.code := Format('c%d', [DataId]);
  data.name := Format('Name_%d', [DataId]);
  data.valid := True;
  Node.SetData(data);
end;

procedure TForm1.Tree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  data: PMyData;
begin
  data := Node.GetData;
  if Assigned(data) then
    data.Free;
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  data: PMyData;
begin
  data := Node.GetData;
  if Assigned(data) then
  case Column of
    1: CellText := data.code;
    2: CellText := data.name;
    3:
      if (data.valid) then
        CellText := 'True'
      else
        CellText := 'False';
  end;
end;

initialization
  DataId := 0;

end.
