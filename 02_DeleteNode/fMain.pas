unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm2 = class(TForm)
    TreeView1: TTreeView;
    btnDelete: TButton;
    procedure btnDeleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
var
  n1, n2: TTreeNode;
begin
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'A');
  TreeView1.Items.AddChild(n1, 'A1');
  TreeView1.Items.AddChild(n1, 'A2');
  TreeView1.Items.AddChild(n1, 'A3');
  TreeView1.Items.AddChild(n1, 'A4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'B');
  TreeView1.Items.AddChild(n1, 'B1');
  n2 := TreeView1.Items.AddChild(n1, 'B2');
  TreeView1.Items.AddChild(n2, 'B21');
  TreeView1.Items.AddChild(n2, 'B22');
  TreeView1.Items.AddChild(n2, 'B23');
  TreeView1.Items.AddChild(n2, 'B24');
  TreeView1.Items.AddChild(n2, 'B25');
  TreeView1.Items.AddChild(n1, 'B3');
  TreeView1.Items.AddChild(n1, 'B4');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'C');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'D');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'E');
  n2 := TreeView1.Items.AddChild(n1, 'E1');
  TreeView1.Items.AddChild(n2, 'E11');
  TreeView1.Items.AddChild(n2, 'E12');
  TreeView1.Items.AddChild(n2, 'E13');
  n2 := TreeView1.Items.AddChild(n1, 'E2');
  TreeView1.Items.AddChild(n2, 'E21');
  TreeView1.Items.AddChild(n2, 'E22');
  TreeView1.Items.AddChild(n2, 'E23');
  TreeView1.Items.AddChild(n2, 'E24');
  TreeView1.Items.AddChild(n1, 'E3');
  n2 := TreeView1.Items.AddChild(n1, 'E4');
  TreeView1.Items.AddChild(n2, 'E41');
  TreeView1.Items.AddChild(n2, 'E42');
  TreeView1.Items.AddChild(n2, 'E43');
  n1 := TreeView1.Items.Add(TreeView1.Selected, 'F');
  TreeView1.Items.AddChild(n1, 'F1');
  TreeView1.Items.AddChild(n1, 'F2');
  TreeView1.Items.AddChild(n1, 'F3');
  TreeView1.Items.AddChild(n1, 'F4');
  TreeView1.FullExpand;
end;

procedure TForm2.btnDeleteClick(Sender: TObject);
var
  ok: Boolean;
  idx: Integer;
begin
  if (TreeView1.Items.Count = 0) then
  begin
    MessageDlg('No item in the list!',
               TMsgDlgType.mtInformation,
               [TMsgDlgBtn.mbOK], 0);
    Exit;
  end;

  if (TreeView1.Selected.HasChildren) then
    ok := (MessageDlg('There are items beneath the selected item. Delete them all?',
                      TMsgDlgType.mtConfirmation,
                      [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes)
  else
    ok := True;

  if (not ok) then
    Exit;

  idx := TreeView1.Selected.AbsoluteIndex - 1;
  TreeView1.Selected.Delete;
  if (idx >= 0) then
    TreeView1.Items[idx].Selected := True;

  TreeView1.SetFocus;
end;

end.
