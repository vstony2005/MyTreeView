unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm6 = class(TForm)
    TreeView1: TTreeView;
    btnSave: TButton;
    edtText: TEdit;
    btnChange: TButton;
    btnOpen: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

uses
  uNote;

procedure TForm6.FormCreate(Sender: TObject);
var
  n1, n2: TTreeNode;
begin
  n1 := TreeView1.Items.AddObject(TreeView1.Selected, 'A', TNote.Create('a'));
  TreeView1.Items.AddChildObject(n1, 'A1', TNote.Create('a1'));
  TreeView1.Items.AddChildObject(n1, 'A2', TNote.Create('a2'));
  TreeView1.Items.AddChildObject(n1, 'A3', TNote.Create('a3'));
  TreeView1.Items.AddChildObject(n1, 'A4', TNote.Create('a4'));
  n1 := TreeView1.Items.AddObject(TreeView1.Selected, 'B', TNote.Create('b'));
  TreeView1.Items.AddChildObject(n1, 'B1', TNote.Create('b1'));
  n2 := TreeView1.Items.AddChildObject(n1, 'B2', TNote.Create('b2'));
  TreeView1.Items.AddChildObject(n2, 'B21', TNote.Create);
  TreeView1.Items.AddChildObject(n2, 'B22', TNote.Create);
  TreeView1.Items.AddChildObject(n2, 'B23', TNote.Create);
  TreeView1.Items.AddChildObject(n1, 'B3', TNote.Create('b3'));
  TreeView1.Items.AddChildObject(n1, 'B4', TNote.Create('b4'));

  TreeView1.FullExpand;
end;

procedure TForm6.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  for i := TreeView1.Items.Count-1 downto 0 do
    TNote(TreeView1.Items[i].Data).Free;
end;

procedure TForm6.btnSaveClick(Sender: TObject);
var
  fs: TFileStream;
  lbl: string[255];
  s: string;
  i, stri, sLen, numOfNodes, nodeLevel: Integer;
begin
  fs := TFileStream.Create('dataTV.otl', fmCreate);
  try
    try
      // write num of nodes in  tv
      numOfNodes := TreeView1.Items.Count;
      fs.WriteBuffer(numOfNodes, SizeOf(numOfNodes));

      // write labels and data of nodes
      for i := 0 to numOfNodes-1 do
      begin
        // write node label
        lbl := TreeView1.Items[i].Text;
        fs.WriteBuffer(lbl, SizeOf(lbl));

        // write node level
        nodeLevel := TreeView1.Items[i].Level;
        fs.WriteBuffer(nodeLevel, SizeOf(nodeLevel));

        // assign txt data to s
        s := TNote(TreeView1.Items[i].Data).Text;
        sLen := Length(s);
        // write len of s
        fs.WriteBuffer(sLen, SizeOf(sLen));

        for stri := 1 to sLen do
          fs.WriteBuffer(s[stri], SizeOf(s[stri]));
      end;
    except
      on E: Exception do
        MessageDlg(e.Message,
                   TMsgDlgType.mtError,
                   [TMsgDlgBtn.mbOK], 0);
    end;
  finally
    fs.Free;
  end;
end;

procedure TForm6.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(Node) then
    edtText.Text := TNote(Node.Data).Text;
end;

procedure TForm6.btnChangeClick(Sender: TObject);
var
  node: TTreeNode;
begin
  node := TreeView1.Selected;
  if Assigned(node) then
    TNote(node.Data).Text := edtText.Text;
end;

procedure TForm6.btnOpenClick(Sender: TObject);

  procedure AddNode(ALastNode: TTreeNode; ALevel: Integer; ATxt: string; ANote: TNote);
  begin
    if (TreeView1.Items.Count = 0) then
      TreeView1.Items.AddChildObject(nil, ATxt, ANote)
    else if (ALevel > ALastNode.Level) then
      TreeView1.Items.AddChildObject(ALastNode, ATxt, ANote)
    else
    begin
      while (ALevel < ALastNode.Level) do
        ALastNode := ALastNode.Parent;

      TreeView1.Items.AddObject(ALastNode, ATxt, ANote);
    end;
  end;

var
  i: Integer;

  note: TNote;
  fs: TFileStream;
  lbl: string[255];
  s: string;
  c: Char;
  j, stri, sLen, numOfNodes, nodeLevel: Integer;
  lastNode: TTreeNode;
begin
  for i := TreeView1.Items.Count-1 downto 0 do
      TNote(TreeView1.Items[i].Data).Free;
  TreeView1.Items.Clear;

  lastNode := nil;
  fs := TFileStream.Create('dataTV.otl', fmOpenRead);
  try
    try
      // read num nodes
      fs.ReadBuffer(numOfNodes, SizeOf(numOfNodes));
      for i := 0 to numOfNodes-1 do
      begin
        // read node label
        fs.ReadBuffer(lbl, SizeOf(lbl));
        // read node level
        fs.ReadBuffer(nodeLevel, SizeOf(nodeLevel));
        // read len of string
        fs.ReadBuffer(sLen, SizeOf(sLen));
        s := '';

        for j := 1 to sLen do
        begin
          fs.ReadBuffer(c, SizeOf(c));
          s := s + c;
        end;

        note := TNote.Create(s);
        AddNode(lastNode, nodeLevel, lbl, note);
        lastNode := TreeView1.Items[TreeView1.Items.Count-1];
      end;
      TreeView1.FullExpand;
    except
      on E: Exception do
        MessageDlg(e.Message,
                   TMsgDlgType.mtError,
                   [TMsgDlgBtn.mbOK], 0);
    end;
  finally
    fs.Free;
  end;
end;

end.
