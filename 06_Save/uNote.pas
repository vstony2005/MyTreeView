unit uNote;

interface

type
  TNote = class
  private
    FText: string;
  public
    constructor Create(const AText: string = '');
    property Text: string read FText write FText;
  end;

implementation

{ TNote }

constructor TNote.Create(const AText: string = '');
begin
  FText := AText;
end;

end.
