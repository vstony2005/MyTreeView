object Form6: TForm6
  Left = 0
  Top = 0
  Caption = 'Form6'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    624
    441)
  TextHeight = 15
  object TreeView1: TTreeView
    Left = 16
    Top = 16
    Width = 585
    Height = 385
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    TabOrder = 0
    OnChange = TreeView1Change
  end
  object btnSave: TButton
    Left = 16
    Top = 409
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Save'
    TabOrder = 1
    OnClick = btnSaveClick
  end
  object edtText: TEdit
    Left = 200
    Top = 410
    Width = 321
    Height = 23
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
  end
  object btnChange: TButton
    Left = 527
    Top = 409
    Width = 75
    Height = 25
    Caption = 'Chan&ge'
    TabOrder = 3
    OnClick = btnChangeClick
  end
  object btnOpen: TButton
    Left = 97
    Top = 409
    Width = 75
    Height = 25
    Caption = '&Open'
    TabOrder = 4
    OnClick = btnOpenClick
  end
end
