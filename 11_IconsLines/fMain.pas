unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, System.ImageList,
  Vcl.ImgList, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    ImageList1: TImageList;
    btnHide: TButton;
    procedure btnHideClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Tree1Collapsed(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1Expanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure Tree1GetImageIndex(Sender: TBaseVirtualTree; Node:
        PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted:
        Boolean; var ImageIndex: TImageIndex);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
  private
    procedure BuildTree;
    procedure ProcessItemDisplay(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  BuildTree;
end;

procedure TForm1.btnHideClick(Sender: TObject);
begin
  Tree1.BeginUpdate;
  try
    Tree1.IterateSubtree(nil, ProcessItemDisplay, nil, []);
  finally
    Tree1.EndUpdate;
  end;
end;

procedure TForm1.ProcessItemDisplay(Sender: TBaseVirtualTree; Node:
    PVirtualNode; Data: Pointer; var Abort: Boolean);
begin
  Sender.IsVisible[Node] := Node.Index < 5;
end;

(*

procedure TfrmMyForm.SearchForText(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
var
  NodeData: PDatastructure; //replace by your record structure
begin
  NodeData := Sender.GetNodeData(Node);
  Abort := AnsiStartsStr(string(data), NodeData.YourFieldHere); //abort the search if a node with the text is found.
end;

procedure TfrmMyForm.myEditChange(Sender: TObject);
var
  foundNode : PVirtualNode;
begin
  inherited;
  //first param is your starting point. nil starts at top of tree. if you want to implement findnext
  //functionality you will need to supply the previous found node to continue from that point.
  //be sure to set the IncrementalSearchTimeout to allow users to type a few characters before starting a search.
  foundNode := vstMyTree.IterateSubtree(nil, SearchForText, pointer(myEdit.text));

  if Assigned (foundNode) then
  begin
    vstMyTree.FocusedNode := foundNode;
    vstMyTree.Selected[foundNode] := True;
  end;
end;

*)

procedure TForm1.Tree1GetImageIndex(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted:
    Boolean; var ImageIndex: TImageIndex);
begin
  case Kind of
    ikState: // for the case the state icon has been requested
      ImageIndex := -1;
    ikNormal:
      ImageIndex := 0;
    ikSelected: // normal or the selected icon is required
      ImageIndex := 1;
  end;
end;

procedure TForm1.BuildTree;
var
  n1, n2: PVirtualNode;
  i, j: Integer;
begin
  for i := 1 to 10 do
  begin
    n1 := Tree1.AddChild(nil);
    for j := 1 to 4 do
    begin
      n2 := Tree1.AddChild(n1);
    end;
  end;

  Tree1.TreeOptions.MiscOptions := Tree1.TreeOptions.MiscOptions + [TVTMiscOption.toCheckSupport];
  Tree1.CheckImageKind := ckSystemDefault;

  n2 := Tree1.AddChild(nil);
  n1 := Tree1.AddChild(n2);
  n1.CheckType := ctNone;
  n1 := Tree1.AddChild(n2);
  n1.CheckType := ctTriStateCheckBox;
  n1 := Tree1.AddChild(n2);
  n1.CheckType := ctCheckBox;
  n1 := Tree1.AddChild(n2);
  n1.CheckType := ctRadioButton;
  n1 := Tree1.AddChild(n2);
  n1.CheckType := ctButton;
  Tree1.Expanded[n2] := True;
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
begin
  CellText := Format('Level %d - Index %d',
                     [Tree1.GetNodeLevel(Node),
                      Node.Index]);
end;

end.
