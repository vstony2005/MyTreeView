unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TForm5 = class(TForm)
    TreeView1: TTreeView;
    edtText: TEdit;
    btnChange: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

uses
  uNote;

procedure TForm5.FormCreate(Sender: TObject);
var
  n1, n2: TTreeNode;
begin
  n1 := TreeView1.Items.AddObject(TreeView1.Selected, 'A', TNote.Create('a'));
  TreeView1.Items.AddChildObject(n1, 'A1', TNote.Create('a1'));
  TreeView1.Items.AddChildObject(n1, 'A2', TNote.Create('a2'));
  TreeView1.Items.AddChildObject(n1, 'A3', TNote.Create('a3'));
  TreeView1.Items.AddChildObject(n1, 'A4', TNote.Create('a4'));
  n1 := TreeView1.Items.AddObject(TreeView1.Selected, 'B', TNote.Create('b'));
  TreeView1.Items.AddChildObject(n1, 'B1', TNote.Create('b1'));
  n2 := TreeView1.Items.AddChildObject(n1, 'B2', TNote.Create('b2'));
  TreeView1.Items.AddChildObject(n2, 'B21', TNote.Create);
  TreeView1.Items.AddChildObject(n2, 'B22', TNote.Create);
  TreeView1.Items.AddChildObject(n2, 'B23', TNote.Create);
  TreeView1.Items.AddChildObject(n1, 'B3', TNote.Create('b3'));
  TreeView1.Items.AddChildObject(n1, 'B4', TNote.Create('b4'));

  TreeView1.FullExpand;
end;

procedure TForm5.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  // (!) le treeview ne d�truit pas les objets, c'est � nous de le g�rer (!)
  for i := TreeView1.Items.Count - 1 downto 0 do
    TNote(TreeView1.Items[i].Data).Free;
end;

procedure TForm5.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  if Assigned(TreeView1.Selected) then
    edtText.Text := TNote(TreeView1.Selected.Data).Text;
end;

procedure TForm5.btnChangeClick(Sender: TObject);
begin
  if Assigned(TreeView1.Selected) then
    TNote(TreeView1.Selected.Data).Text := edtText.Text;
end;

end.
