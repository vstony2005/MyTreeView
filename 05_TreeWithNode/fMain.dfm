object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    624
    441)
  TextHeight = 15
  object TreeView1: TTreeView
    Left = 16
    Top = 16
    Width = 585
    Height = 377
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    TabOrder = 0
    OnChange = TreeView1Change
  end
  object edtText: TEdit
    Left = 16
    Top = 408
    Width = 504
    Height = 23
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
  end
  object btnChange: TButton
    Left = 526
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Change'
    TabOrder = 2
    OnClick = btnChangeClick
  end
end
