unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees;

type
  TForm1 = class(TForm)
    Tree1: TVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
        PVirtualNode; var InitialStates: TVirtualNodeInitStates);
  private
    procedure BuildTree;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

type
  PMyData = ^TMyData;
  TMyData = record
    id: Integer;
    code: string[15];
    name: string[80];
    level: Integer;
    valid: Boolean;
  end;

{ TForm1 }

procedure TForm1.BuildTree;
var
  col: TVirtualTreeColumn;
begin
//  Tree1.NodeDataSize := SizeOf(TMyData);

  col := Tree1.Header.Columns.Add;
  col.Text := 'Column 1';
  col.Width := 150;
  col := Tree1.Header.Columns.Add;
  col.Text := 'Column 2';
  col.Width := 150;
  col := Tree1.Header.Columns.Add;
  col.Text := 'Column 3';
  col.Width := 150;
  col := Tree1.Header.Columns.Add;
  col.Text := 'Column 4';
  col.Width := 150;

  Tree1.RootNodeCount := 10;

  Tree1.Header.Options := Tree1.Header.Options
                          + [TVTHeaderOption.hoVisible];   // afficher colonnes

  Tree1.FullExpand;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  BuildTree;
end;

procedure TForm1.Tree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
begin
  CellText := Format('%d %d', [Node.Index, Column]);
end;

procedure TForm1.Tree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node:
    PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  if not Assigned(ParentNode) then
    Tree1.ChildCount[Node] := 5;
end;

end.
