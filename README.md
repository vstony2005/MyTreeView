# Treeview

Test Treeview with Delphi 11.3.

## Projects

### 1_CreateNote

Add node and add childs in Treeview.

### 2_DeleteNode

Create a Treeview tree and delete nodes.

### 3_OutInDent

Outdent and Indent nodes.

### 4_DragNDrop

Drag and drop nodes.

### 5_TreeWithNode

Use objects in Treeview.

### 6_Save

Save and open Treeview datas.

### 7_VirtualTV

Using Virtual Treeview.

### 8_VTV

Create a Treeview tree and delete nodes with Virtual Treeveiw. Using objects.

### 9_VTV_init

Buils Tree Node and children without object.

### 10_DatasDB

Read datas from database (SQL Server). Group datas.
Using text color and images.

### 11_IconsLines

Icon on line and selected line.

### 12_Checkbox

Lines with checkbox. Check/uncheck sub-lines.
Icons  for line collapsed/expanded.

### 13_TreevewVCL

Creates operation in list and show  them in a Treeview.

First version using:

```pascal
procedure TFrmMain.treevGetCellText(Sender: TCustomVirtualStringTree; var E:
    TVSTGetCellTextEventArgs);
var
  calc: PCalculElt;
begin
  calc := Sender.GetNodeData(E.Node);
  if not Assigned(calc) then
    e.CellText := 'Error'
  else if (calc.Level = 0) then
    e.CellText := 'Error level'
  else if (calc.Level = 1) and (E.Column = 0) then
    e.CellText := calc.Description
  else if (calc.Level = 2) then
    case E.Column of
      0:
        E.CellText := calc.N1.ToString;
      1:
        E.CellText := calc.Operation;
      2:
        E.CellText := calc.N2.ToString;
      3:
        E.CellText := '=';
      4:
        E.CellText := FloatToStr(calc.Resultat);
    else
      E.CellText := 'Empty';
    end
  else
    E.CellText := '';
end;
```

### 14_TreeEdit

Edit field in TreeView.

## Library

- [JAM-Software/Virtual-TreeView](https://github.com/JAM-Software/Virtual-TreeView)
- [Northwind DB](https://github.com/microsoft/sql-server-samples/blob/master/samples/databases/northwind-pubs/instnwnd.sql)
- [Icons](https://www.fatcow.com/free-icons)
