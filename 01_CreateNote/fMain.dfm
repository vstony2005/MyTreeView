object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  DesignSize = (
    624
    441)
  TextHeight = 15
  object TreeView1: TTreeView
    Left = 16
    Top = 16
    Width = 585
    Height = 377
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    TabOrder = 0
  end
  object btnAddRoot: TButton
    Left = 16
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Add Node'
    TabOrder = 1
    OnClick = btnAddRootClick
  end
  object btnAddChild: TButton
    Left = 112
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Add Child'
    TabOrder = 2
    OnClick = btnAddChildClick
  end
end
