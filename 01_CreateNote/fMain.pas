unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    TreeView1: TTreeView;
    btnAddRoot: TButton;
    btnAddChild: TButton;
    procedure btnAddChildClick(Sender: TObject);
    procedure btnAddRootClick(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnAddRootClick(Sender: TObject);
var
  node: TTreeNode;
begin
  node := TreeView1.Items.Add(TreeView1.Selected, 'NewItem');
  node.Selected := True;
  node.EditText;
end;

procedure TForm1.btnAddChildClick(Sender: TObject);
var
  node: TTreeNode;
begin
  node := TreeView1.Items.AddChild(TreeView1.Selected, 'NewItem');
  node.Selected := True;
  node.EditText;
end;

end.
